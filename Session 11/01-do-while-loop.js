












/*
    The Do-While Loop


    The do-while loop is extremely similar to the while loop that we learned
    in the previous section.  The main difference between the two is a
    do-while loop evaluates the condition *after* the loop completes.  This
    means the do-while loop will always execute atleast one time before
    exiting.


*/


























/*

    The Do-While loops syntax


    do {

        <some code you want to execute 1 or more times>;

    } while (<conditional statement>);

*/


// NOTE the semi-colon at the end.



















// function sing99BottlesOfBeer () {



//     const jsOutput = document.querySelector("#jsOutput");
//     jsOutput.innerHTML = '<ul>';
//     let bottlesOfBeer = 99;

//     do {
        
//         jsOutput.innerHTML += `
//             <li>
//                 ${bottlesOfBeer} bottles of beer on the wall, ${bottlesOfBeer}
//                 bottles of beer.
                
//                 You take one down, pass it around.  ${-- bottlesOfBeer}
//                 bottles of beer on the wall.
//             </li>
//         `;

//     } while (bottlesOfBeer > 0);

//     jsOutput.innerHTML += '</ul>';



// }

// sing99BottlesOfBeer();


// NOTE     -- bottlesOfBeer?  WTF?!



















// function break99BottlesOfBeer () { 

//     const jsOutput = document.querySelector("#jsOutput");
//     jsOutput.innerHTML = '<ul>';
//     let bottlesOfBeer = 99;

//     do {

//         jsOutput.innerHTML += `
//             <li>
//                 ${bottlesOfBeer} bottles of beer on the wall, ${bottlesOfBeer}
//                 bottles of beer.
                
//                 You take one down, pass it around.  ${-- bottlesOfBeer}
//                 bottles of beer on the wall.
//             </li>
//         `;
//         break;

//     } while (bottlesOfBeer > 0);

//     jsOutput.innerHTML += '</ul>';

// }

// break99BottlesOfBeer();





























// function continue99BottlesOfBeer () { 

//     const jsOutput = document.querySelector("#jsOutput");
//     jsOutput.innerHTML = '<ul>';
//     let bottlesOfBeer = 99;

//     do {

//         if (bottlesOfBeer % 10 !== 0)  {
//             bottlesOfBeer --;
//             continue;
//         }

//         jsOutput.innerHTML += `
//             <li>
//                 ${bottlesOfBeer} bottles of beer on the wall, ${bottlesOfBeer}
//                 bottles of beer.
                
//                 You take one down, pass it around.  ${-- bottlesOfBeer}
//                 bottles of beer on the wall.
//             </li>
//         `;

//     } while (bottlesOfBeer > 0);

//     jsOutput.innerHTML += '</ul>';



// }

// continue99BottlesOfBeer();



// NOTE the modulus operator... who remembers it from session 01?











/* Exercise

    1. Similar to our previous sessions homework, create a JavaScript/HTML app
            with a function that takes 2 parameters, an array and a key.
    
    2. In that function use the "index method" to step through all of the
            elements in the array using a do-while loop.
    
    3. Check each element from the array to see if it equals the value of the
            key provided when the function is called.

    4. If the current element and the key are equal, increment the index and
            use the continue keyword to skip it.
    
    5. If the current element and the key are not equal, output the element to the
            console and increment the index.

*/




































// // An aside to discuss events


// const jsOutput = document.querySelector("#jsOutput");
// jsOutput.innerHTML = `
//     <button type="button" id="theButton">Click Me!</button>
// `;

// const theButton = document.querySelector("#theButton");

// async function getMovies() {
//     const response = await fetch('movies.json');
//     const movies = await response.json()
//     console.log(movies);
// }

// theButton.addEventListener('click', getMovies);




// // NOTE https://developer.mozilla.org/en-US/docs/Web/Events
// // NOTE theButton.addEventListener()
// // NOTE fetching JSON from our own site






/* Homework

    1. Create a json file either an array or an object that contains 
        an array.

    2. Create an HTML file that includes a: 
            - button element
            - an (ordered, or unordered) list element where we can write the output of our 
        function
    
    3. Create a JavaScript file with an async function that fetch's
        that json file and parses it into a usable javascript object.

    4. Using a do-while loop, step through the array from fetch'd 
        json data.  During each iteration of the loop, append another list
        item element to the list with the contents of the array element.

    5. Outside of the function, add an event listener to the button that
        will execute our function when the button is clicked.

    6. Test your app and validate the output.

    7. Test your app again by clicking multiple times.  Do more and more
        elements continue to stack up?  If so, see if you can figure out the 
        code needed so that only fresh data appears when you click the button.


*/












































/* 
    Throwing an error....

    Imagine you have a function that retrieves some data from the web.  One
    time, when the function is called, the website was unavailable the
    function is unable to return the data it needs to.

    How can this function alert the code that is calling this function that
    this issue has occured?  We can return a null value, or a string
    explaining the situation... but that would mean the code that calls our
    function would need to include additional logic to determine if the data
    being returned is an error, or if its the data we requested...

    JavaScript includes the "throw" keyword for this precise situation.

    Pseudo-code:

        throw <Error class object>;

*/























// // Real example of using the throw keyword....
// function greetSteve(name) {
//     if (name !== 'Steve') {

//         throw new Error("This isn't Steve, I only greet Steve.");

//     }
//     console.log(`Hey ${name}!`);
// }

// // Pass in a number to cause an error.
// greetSteve("Patrick");





// // NOTE give a brief explanation of class objects (the new keyword, the
// //      message argument, etc).

// // NOTE when an error is "thrown", the greetSteve function stops immediately
// //      and returns back to line 65 (where greetSteve() is called).

// // NOTE we don't move past line 65 after the error is thrown if we don't
// //      handle the error. 



















// Exercise... write a function that throws an exception and call the function.


// throw new Error("message")

























//  Built-in error classes

//         RangeError
//              The RangeError object indicates an error when a value is not
//              in the set or range of allowed values.

//         ReferenceError
//              The ReferenceError object represents an error when a variable
//              that doesn't exist (or hasn't yet been initialized) in the
//              current scope is referenced.

//         SyntaxError
//              The SyntaxError object represents an error when trying to
//              interpret syntactically invalid code. It is thrown when the
//              JavaScript engine encounters tokens or token order that does
//              not conform to the syntax of the language when parsing code.

//         TypeError
//              The TypeError object represents an error when an operation
//              could not be performed, typically (but not exclusively) when a
//              value is not of the expected type.

//         URIError
//              The URIError object represents an error when a global URI 
//              handling function was used in a wrong way

















// // Cause a reference error
// function causeReferenceError() {
//     console.log(nonExistentVariable);
// }

// causeReferenceError();




// // Cause a range error
// function causeRangeError() {
//     const array = [];
//     array.length = -1;
// }

// causeRangeError();




// // Cause a syntax error
// function causeSyntaxError() {
//     blah blah;
// }

// // NOTE This one causes an exception before it's even called.  This is why we
// //      can't "catch"/handle a syntaxerror.




// // Cause a type error
// function causeTypeError() {
//     const blah = null;
//     blah.f();
// }

// causeTypeError();
















/*

    How to handle an error using a try/catch block

    pseudo-code

    try {
        
        <some code you want to run that might cause an error>;

    } 
    
    catch (<the name of the error object if one is thrown>) {

        <code that will run if an error is thrown>;

    }

*/


















// function throwAnError() {
//     throw new Error("The thowAnError() function has been called.");
// }


// try {
//     throwAnError();
// } 

// catch (error) {
//     console.log(error instanceof Error);
//     console.log(error.message);
// }

// console.log('Notice that this line is also run.');


// // NOTE we haven't talked about instanceof yet... this asks if an object
// //      happens to be of a particular class.






















// Exercise... 

// Write a function that intentionally causes an error.  You can
// either throw an error directly, or you can cause an error via one of the
// methods I demonstrated earlier.  Call the function from inside of a 
// try/catch and handle the error so the program can continue to process.















// // How to catch only errors of a specific type....


// function causeRangeError() {
//     [].length = -1;
// }

// try {

//     causeRangeError();

// } catch (e) {

//     if (e instanceof RangeError) {
//         console.log("It's just a range error, nothing to see here...");
//     } else {
//         throw e;
//     }

// }

// console.log("This also happens.");


// // NOTE Show what happens when you throw an error that isn't a RangeError.

















// Exercise

// Write and call a function that throws a ReferenceError.

// Using a try/catch block, catch the error and check if it's a ReferenceError: 
//      If so log a message and continue.
//      If not, rethrow it.

// Test your code by checking the console log. Also test by causing a non-ReferenceError
//      to be thrown in your function.


// NOTE You can use "new ReferenceError()" or you can cause a reference error by
//      refering to an object's property that doesn't exist.

// throw new ReferenceError("This is my reference error")


















// Finally!

// What if we want some code to happen no matter what, regardless of errors being thrown (or not)?

// This is what the finally block is for....



function causeRangeError() {
    [].length = -1;
}

try {
    console.log("Calling a function that will error.");
    // causeRangeError();
} 

catch (e) {
    console.log("We caught the error, hooray!");
}

finally {
    console.log('Finally, this happens no matter what!');
}

console.log("This happens outside of the try/catch/finally block.");


// NOTE Demonstrate what happens if the error doesn't occur.
// NOTE Demonstrate what happens if we catch the error.













/*
    HOMEWORK - Research Promises, async and await

    Answer these questions....

    1. What is a promise and what i s it used for?
    2. What does Promise.then(), Promise.catch() and Promise.finally() do?
    3. What does "await" do and how does it related to Promises?
    4. What does "async" do and how does it relate to "await"?

*/






/* 

For-in loop

A for-in loop is a convenient and syntactically easy to read/understand method
for "looping" through all of the properties in an object.

Below is the pseudo-code for using the for-in loop.

    const object = {
        ...
    }

    for (<variable> in <object>) {
        // Loop one time for every property in <object>
        //   setting value of <variable> to the name of a
        //   property during each iteration.

        <code to run during the iteration>;
    }


*/




















// function displayPepperObject() {
//     const jsOutput = document.querySelector("#jsOutput");

//     const pepper = {
//         type: "bell",
//         color: "red",
//         countryOfOrigin: "US",
//         weigth: "3",
//         weightUnit: "oz"
//     };

//     // <ul></ul>
//     const ul = document.createElement("ul");

//     for (const prop in pepper) {
//         // <li></li>
//         let li = document.createElement("li");
//         li.innerHTML = `${prop} = ${pepper[prop]}`;
//         ul.appendChild(li);

//     }
//     jsOutput.appendChild(ul);
// }

// displayPepperObject();



















/* 

For-of loop

A for-of loop is a convenient and syntactically easy to read/understand method
for "looping" through all of the elements in an array.

Below is the pseudo-code for using the for-in loop.

    const array = [
        ...
    ]

    for (<variable> of <array>) {
        // Loop one time for every element in <array>
        //   setting value of <variable> to the value of the
        //   element during each iteration.

        <code to run during the iteration>;
    }


*/




















function displayArrayOfPepperObjects() {
    const jsOutput = document.querySelector("#jsOutput");
    const peppers = [
        {
            type: "bell",
            color: "red",
            countryOfOrigin: "US",
            weigth: "3",
            weightUnit: "oz"
        },
        {
            type: "jalapeño",
            color: "green",
            countryOfOrigin: "Mexico",
            weigth: "1",
            weightUnit: "oz"
        },
    ];
    const ul = document.createElement("ul");

    for (const pepper of peppers) {
        let li = document.createElement("li");

        for (const prop in pepper) {
            li.innerHTML += `${prop} = ${pepper[prop]}<br>`;
        }
        
        ul.appendChild(li);
    }
    jsOutput.appendChild(ul);
}

displayArrayOfPepperObjects();

























/*

    Classes

    Classes are a template for creating objects, they encapsulate data with
    code (methods) that are used to do work on that data.


*/








// // Defining a basic class

// class Customer {
//     constructor(first, last, email) {
//         this.firstName = first;
//         this.lastName = last;
//         this.email = email;
//     }
// }

// // Instantiating a new object of the Custom class.

// const newCustomer = new Customer('Steve', 'Perry', 'steve.perry@journey.com');
// console.log(newCustomer);




// NOTE the capitalized class name as a convention
// NOTE the non-capitalized `newCustomer` object
// NOTE the "this" variable
// NOTE the constructor method and it's arguments
// NOTE the use of the new keyword








// Defining a class with methods

// class Rectangle {
//     constructor(width, height) {
//         this.width = width;
//         this.height = height;
//     }

//     calculateArea() {
//         return this.width * this.height;
//     }

//     calculatePerimiter() {
//         return this.width*2 + this.height*2;
//     }
// }

// const rect1 = new Rectangle(2, 5);
// const rect2 = new Rectangle(4, 6);


// console.log(rect1.calculateArea());
// console.log(rect2.calculateArea());
// console.log(rect2.calculatePerimiter());


// NOTE the use of 'this' in the calculateArea() method









// Exercise

// 1. Define a class of your choosing
// 2. Write a constructor method that takes parameters
//       and assigns them to instance properties/variables
// 3. Write another method to do *something* with those
//       instance variables
// 4. Instantiate an object from your class.
// 5. Call your instance method.

















// Setting the value of instance variables....

// class Rectangle {
//     constructor(width, height) {
//         this.width = width;
//         this.height = height;
//     }

//     calculateArea() {
//         return this.width * this.height;
//     }
// }

// const rect1 = new Rectangle(2, 5);
// rect1.width = 3;

// console.log(`Area of rect 1 is ${rect1.calculateArea()}`);
// console.log(`The height of rect 1 is ${rect1.height}`);



// NOTE you can read and assign values to instance properties directly
// NOTE this can cause a problem though... what if the value
//      makes no sense or isn't even the correct type?











// Using getter and setter methods....

// class Rectangle {
//     constructor(width, height) {
//         this._width = width;
//         this._height = height;
//     }
    
//     get area() {
//         return this._width * this._height;
//     }

//     set width(val) {
//         if(typeof(val) !== 'number') {
//             throw new TypeError("width property needs to be a number.");
//         }
//         this._width = val;
//     }
// }

// const rect1 = new Rectangle(2, 5);
// rect1.width = 'a';

// console.log(`Area of rect1 is ${rect1.area}`);






// NOTE demonstrate this using the newer get / set keywords
// NOTE the underscore property names used in the constructor...
// NOTE getters/setters aren't always needed but are good for when
//      you want to sanity check the value of a particular parameter
//      you want to take action when a value is assigned









class Car {
    constructor(make, model, year, image) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.image = image;
    }

    toString() {
        return `
            <h1>${this.year} ${this.make} ${this.model} </h1>
            <img src="${this.image}" />
        `;
    }
}

const car = new Car("Ford", "Focus", "2000", "https://cdne.carbuzz.com/img.vast.com/1181568910446828217/1/640x480");
console.log(car);
console.log(`This is a car object: ${car}`);
const car1 = new Car("Ford", "Focus", "2001", "https://cdne.carbuzz.com/img.vast.com/1181568910446828217/1/640x480");
const car2 = new Car("Ford", "Focus", "2002", "https://cdne.carbuzz.com/img.vast.com/1181568910446828217/1/640x480");
const car3 = new Car("Ford", "Focus", "2003", "https://cdne.carbuzz.com/img.vast.com/1181568910446828217/1/640x480");

const jsOutput = document.querySelector("#jsOutput");
jsOutput.innerHTML = `
    <ul>
        <li>${car}</li>
        <li>${car1}</li>
        <li>${car2}</li>
        <li>${car3}</li>
    </ul>
`;











// Homework

// 1. Create a Circle class
// 2. Add a constructor method with a radius parameter/argument
// 3. Define a getter method that returns the area of the circle
//      (Math.PI * radius ** 2)
// 4. Define a setter method that allows us to update the value of radius
// 5. Create an instance of a circle, use console.log to display the area
// 6. Test the setter method and console.log the updated area.
// 7. Add a toString method and render the details of the circle to HTML.










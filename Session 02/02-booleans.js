


















// Booleans: A variable type in javascript that can be either true or false.































// // Demonstration of how to assign boolean types to variables directly
// const the_big_lebowski_is_the_best_movie_ever = true;
// const keith_richards_can_be_killed_by_conventional_weapons = false;

// console.log('The Big Lebowski is the best movie ever: ' + the_big_lebowski_is_the_best_movie_ever);
// console.log('Keith Richards can be killed by conventional weapons: ' + keith_richards_can_be_killed_by_conventional_weapons);








































// // Demonstrate how to create a boolean variable using a comparison operators
// const back_to_the_future1 = 10;
// const back_to_the_future2 = 8;
// const the_hunt_for_red_october = 10;

// // // Check if "Back to the Future" is equal-to "Back to the Future Part 2" (obviously false)
// // //      Note the == operator being used below.  It calculates if 2 variables have the same value.
// let result = back_to_the_future1 == back_to_the_future2;
// console.log('Is back to the future 1 equal-to back to the future 2? ' + result);





// // Check if "Back to the Future" is not-equal-to "Back to the Future Part 2" (obviously true)
// //      Note the != operator being used below.  It calculates if 2 variables have different values.
// result = back_to_the_future1 != back_to_the_future2;
// console.log('Is back to the future 1 not-equal-to back to the future 2? ' + result);





// // Check if Back to the Future is greater than Back to the Future Part 2 (obviously true)
// //      Note the > operator being used below.  It calculates if the 1st variables is greater-than the second.
// result = back_to_the_future1 > back_to_the_future2;
// console.log('Is back to the future 1 greater-than back to the future 2? ' + result);





// // Check if Back to the Future Part 1 is less-than Back to the Future Part 2 (obviously false)
// //      Note the < operator being used below.  It calculates if the 1st variables is less-than the second.
// result = back_to_the_future1 < back_to_the_future2;
// console.log('Is back to the future 1 less-than back to the future 2? ' + result);




// // Check if Back to the Future Part 1 is greater-than or equal to The Hunt for Red October
// //      Note the >= operator being used below.  It calculates if the 1st variables is greater-than OR equal-to the second.
// result = back_to_the_future1 >= the_hunt_for_red_october;
// console.log('Is back to the future 1 greater-than or equal-to The Hunt for Red October? ' + result);




// // Check if Back to the Future Part 1 is less-than or equal to The Hunt for Red October
// //      Note the <= operator being used below.  It calculates if the 1st variables is greater-than OR equal-to the second.
// result = back_to_the_future2 <= the_hunt_for_red_october;
// console.log('Is back to the future 2 less-than or equal to the hunt for red october? ' + result);
















































// // What do you think is output to the console with this snippet of code?
// const n = 10;                // assign the number 10 to the variable named n
// console.log(n == '10');      // notice that we are comparing 10 to '10', a string type

























// // Let's re-write this such that string and number values aren't equal using the '===' operator 
// // (strict equals operator).
// const n = 20;
// console.log(n === '20')  // This returns false as we would expect




























// // More examples where == operator has unexpected results

// // Historically (with other programming languages where boolean types didn't exist) the number 1 would stand-in 
// // for true, and 0 would stand-in for false.  Javascript continues that notation with the == operator.
// console.log(1 == true);
// console.log(0 == false);

// // With the strict equals operator this convention is not used.
// console.log(1 === true);
// console.log(0 === false);

























// // With the regular equals operator empty strings are corced into being false
// console.log('' == false);
// console.log('fdsa' == false);

// // With the strict comparison operator strings are always false when compared with a boolean value
// console.log('' === false)          // This is false
// console.log('' === true)           // This is false
// console.log('non-empty' === false) // This is false
// console.log('non-empty' === true)  // This is false
















// // There are also "notted" versions of the comparison and strict-comparison operator 
// 10 != 10                    // This statement can be read as follows... 10 does-not equal 10.  This evaluates to false
// console.log(10 != 10);      // ... as shown here


// console.log(10 != '10');    // This shows non-strict behavior of the comparison operator.  This evaluates to false
// console.log(10 !== '10');   // This shows strict behavior of the comparison operator.  This evaluates to true



























// Safety tip: Use the strict comparison operator in most cases.  Only changing if you intend to use the alternate functionality of a non-strict comparison


























































// Exercise: 

// Write down what you think the result of each comparison operation will be...  

// #1
//      10 == (5 + 5)

// #2
//      const s = 'Hello world!';
//      s.length >= 12

// #3
//      10 < 10

// #4
//      10 <= 10

// #5
//      50 - 25 === 25

// #6
//      10 + 10 == '20'

// #7
//      0 == false

// #8
//      1 === true























// Review:
//
//  ==       Equals operator
//  ===      Strict equals operator
//  !=       Not-equals operator
//  !==      Strict not-equals operator
//  >        Greater-than operator
//  <        Less-than operator
//  >=       Greater-than or equal-to operator
//  <=       Less-than or equal-to operator
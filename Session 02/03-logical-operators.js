// // The logical and operator (&&) compares 2 boolean expressions to evaluate if both result in a true value.
// const n = 10;
// const s = 'hullabaloo';

// // Result is true, if both expresions are true
// //      Note: you can write this statement without the parenthesis, but it makes it *much* harder to read
// const result = (n === 10) && (s === 'hullabaloo');
// console.log(result);































// // The logical or operator (||) compares 2 boolean expressions to evaluate if either results in a true value.
// const n = 10;
// const s = 'hullabaloo';

// // Result is true, if both expresions are true
// //      Note: you can write this statement without the parenthesis, but it makes it *much* harder to read
// const result = (n === 10) || (s === 'hullabaloo');
// console.log(result);



























// //The NOT (!) operator returns the reverse result of a boolean expression

// const w = !true;            // The variable w equals false because the not (!) operator negated the true statement
// const x = !(5 + 5 === 10)   // The statement in the parenthesis evaluates to true, but the not (!) operator negates, so x equals false

// const y = !false;           // The variable y equals true because the not (!) operator negated the false statement
// const z = !(5 + 5 === 32)   // The variable z equals true because the not (!) operator negated the false statement





























// Take-home Exercise:
//      1. Think of a category of things in your life that you can easilly rank
//      (ranking of favorite movies, top-speeds of various cars, size of fruit...).
//      2. Using that category create 3 to 5 variables with names of things in that
//      category along with whatever value you wish to assign.
//      3. Using these variables, demonstrate the use of each comparison 
//      and logical operator from this session.
//          i.e. ==, ===, !=, !==, <, >, <=, >=, &&, || and !
//      4. For each operator, write a snippet of code that uses it to assign a 
//         boolean variable to a result.
//      5. Log the result along with a description of what you were testing.
/*
    Session 01 review/notes

        // How to define a new variable
            let name = 'Patrick';

        // How to assign a value to a variable that had been previously defined with the 'let' operator
            name = 'Steve';

        // How to use the '+' operator to construct a string that includes a variable
            let greeting = 'Hello ' + name + 'how are you this fine evening?';

        // How to output something to the console
            console.log('Something!');

    Session 01 Review Exercise
        0. Create a new HTML and JS file called 01-session01-review.js and 01-session01-review.html and 
            link them together with the <script> tag
        1. Assign variable 'a' the number value 10 
        2. Assign variable 'b' the number value 20
        3. Swap the values of 'a' and 'b' using a third variable 'c' as temporary storage during the 
            swap.
        4. Output a string to the console that says: "The value of variable a is (the value of a) and 
            variable b is (the value of b)"
        5. Right-click on your html file and open with live server to check your answer, fix any 
            bugs/errors found in the output.
        5. Extra point: Include a single-line comment above one or more of the JS "statements" 
            explaining what is happening in that step
        6. Extra point: If you include a multi-line comment at the top of the file explaining that this
            is a review of Session 01
*/













// Patrick's solution below --------------------------v











/*
    This is Patrick's solution to the review exercise from
    session 01.
*/

// Define variable a with the value 10
let a = 10;

// Define the variable b with the value 20
let b = 20;

// Define the variable c and assign the value of a so we don't lose it while making the swap.
let c = a;

// Reassign the value of a to be the initial value of b
a = b;

// Reassign the value of b to be the initial value of a (temporarilly stored in variable c)
b = c;

// Write the values of a and b to the console.log()
console.log('The value of variable a is ' + a + ' and variable b is ' + b);
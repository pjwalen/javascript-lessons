











// How a web browser works

// 1. A user types in a URL (http://www.google.com/) at the top of their web browser and hits enter.

// 2. Your web browser calls up the 411 of the internet, one of the domain name servers (DNS) and says
//        Some user just asked me to of "google.com", what's their telephone number? (AKA IP Address)
//

// 3. DNS responds to that request with google's IP address

// 4. Our web browser calls up that IP address (makes a socket connection to it) and initiates a discussion 
//        (literally sends string data in an agreed upon format to request data).  The agreed-upon format
//        of this text is known as Hyper-Text Transport Protocol (HTTP)

// 5. The web server (google.com) responds to us in-kind with the data we requested.

// NOTE: Let's see this in action, by playing the part of a web browser and sending google a request using telnet.


























// The structure of an HTTP request

/* Our request......

    GE/T  HTTP/1.1
    Host: www.google.com

*/

// NOTE the request method (GET)
// NOTE discuss the other methods (POST, PUT, DELETE)
// NOTE our path (/)
// NOTE the host header

/* Their response......

    HTTP/1.1 200 OK
    Date: Thu, 09 Feb 2023 07:53:48 GMT
    Expires: -1
    Cache-Control: private, max-age=0
    Content-Type: text/html; charset=ISO-8859-1
    P3P: CP="This is not a P3P policy! See g.co/p3phelp for more info."
    Server: gws
    X-XSS-Protection: 0
    X-Frame-Options: SAMEORIGIN
    Set-Cookie: 1P_JAR=2023-02-09-07; expires=Sat, 11-Mar-2023 07:53:48 GMT; path=/; domain=.google.com; Secure
    Set-Cookie: AEC=ARSKqsKxZOaPO2fTlqPcA0HkTFg0TdG5pq3b4sgz37fzlYaJNw-0M5qSZ_U; expires=Tue, 08-Aug-2023 07:53:48 GMT; path=/; domain=.google.com; Secure; HttpOnly; SameSite=lax
    Set-Cookie: NID=511=R_oO6QocLbpfJjp4jNCR6kjwSKg7brxqkTJ9h2_OEGyGUZtJch3Xk8w9CTsXS-E5Mi4yVkp5yniPvvixbFDaUk4FYrFTvG4ODT-1-UG0gDNFu37cV-kqmV3cjT_toOAAm0VYwX0gBbcw2KLaPuEzO8AKaph93kPw4VXLplexfbY; expires=Fri, 11-Aug-2023 07:53:48 GMT; path=/; domain=.google.com; HttpOnly
    Accept-Ranges: none
    Vary: Accept-Encoding
    Transfer-Encoding: chunked

    5b87
    <!doctype html><html itemscope="" itemtype="http://schema.org/WebPage" lang="en"><head><meta content="Search the world's information, including webpages, images, videos and more. Google has many special features to help you find exactly what you're looking for." name="description"><meta content="noodp" name="robots"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"><meta content="/images/b

*/

// NOTE the status code and the status text (talk about the various codes, including 404)
// NOTE the Content-Type, and Set-Cookie header (and the whole header block)
// NOTE the body (the HTML at the bottom)

















// The GutenDex API (https://gutendex.com/)
//
// The GutenDex API is free-to-use API for looking up books that are in the public domain


// Retrieve a list of all books in the index (only returns a limited number 
//           of books per page).
//
// GET https://gutendex.com/books/ 
//
//                       NOTE the count, next, previous and results object properties
//                       NOTE the book object that is a member of the results array
//                       NOTE the id in the book object
//                       NOTE the url query parameters in documentation



// Retrieve an individual book record by its id.
// GET https://gutendex.com/books/<id>
//
//                      NOTE Demonstrate querying for - F. Scott Fitzgerald





























// So how do we get our web browser to request data (perform a GET method) on a website?

// The fetch() function......

/* You can run fetch in the manner shown below to retrieve data from a website
        using the GET method (the default method).

  fetch(<url>)

        <url> - in this case, is a string that provides the URL we 
                    are trying to retrieve
/*


/*  In this next form, we are passing fetch 2 arguments.....

  fetch(<url>, <options>)

        <url>     - A string that provides the URL we are trying to retrieve.
        <options> - An object that provides our connection options, such as a
                        method other than GET, or custom headers amongst others.

*/
















// async function queryForBook(topic) {

//     // Declare our normal jsOutput
//     jsOutput = document.querySelector('#jsOutput');

//     // Fetch the data located at the following URL and return a response object
//     const response = await fetch("https://gutendex.com/books?topic=" + topic);

//     // Let's examine the response object
//     console.log('Response Object: ', response);
//     console.log('Response Status: ', response.status);
//     console.log('Response Status Text: ', response.statusText);
//     console.log('Response Header - Content-Type: ', response.headers.get("Content-Type"));
//     // const books = await response.text();
//     const books = await response.json();
//     console.log('JSON Data Returned: ', books);
//     // console.log('JSON Data Returned: ', books.count);
//     // console.log('JSON Data Returned: ', books.results);
// }

// queryForBook("poverty");















// Exercise - Define an async function to query the gutendex API and query for all 
//         books of any topic. (Use the API docs to determine how to query on a topic).
//         decode the results into an object (using response.json()).

//         console.log the number of results

//         console.log the array from the first page of results




















// Let's query an individual book and display some data

// async function queryBookText(id) {
//     // Declare our normal jsOutput
//     jsOutput = document.querySelector('#jsOutput');

//     // Fetch the data located at the following URL and return a response object
//     let response = await fetch("https://gutendex.com/books/" + id);

//     // Let's examine the JSON
//     const book = await response.json();
//     console.log('JSON Data Returned: ', book);

//     // Get the URL where the books text is being stored
//     const title = book.title;
//     const authorsName = book.authors[0].name;
//     const bookTextUrl = book.formats['text/plain'];
//     const bookCoverUrl = book.formats['image/jpeg'];

//     jsOutput.innerHTML = `
//         Title: ${book.title}
//         <br>
//         Author: ${authorsName}
//         <br>
//         <a href="${bookTextUrl}">
//             <img src="${bookCoverUrl}" />
//         </a>
//     `;
// }

// queryBookText(64317);


















// Homework - 
//
// 1. Create an HTML/JS app

// 2. Write an async function that takes an author/string 
//      parameter and uses it to search the gutendex API 
//      for all of the books that match the authors name.
//      Call this function authorSearch(author).

// 3. In the same file declare another function that we
//      could pass into the .forEach(<callback>) method
//      of results array.  That function should  have
//      a parameter called book.  Each time the callback
//      method is called, it will append a div to jsOutput
//      with the following contents:
//               - Title of the book
//               - Authors name
//               - An image of the books cover, that when 
//                   clicked links to the text of the book 
//                   on the gutenberg website.

// 4. In the authorSearch() function, call the .forEach
//      method of the results array.

// 5. In the body of your javascript file, call authorSearch()
//      with the name of an author.



function capitalize(str) {
    console.log(str.toUpperCase());
}


function functionOne() {
    const blah = ["one", "two", "three", "four"];

    blah.forEach(capitalize);
}

functionOne();
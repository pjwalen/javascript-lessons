
















// Just like everything in in JavaScript there are a multitude of ways to do
// the same thing... 





















// // This is an example of a class declared the same way we've previously
// // declared classes
// class Vehicle1 {
//     constructor(topSpeed) {
//         this.topSpeed = topSpeed;
//     }

//     go() {
//         console.log(`${this.constructor.name} is going at ${this.topSpeed} mph`);
//     }
// }

// const vehicle1 = new Vehicle1(10);
// vehicle1.go();


// // NOTE the this.constructor.name variable











// // This is an example of declaring a class using a class-expression.
// const Vehicle2 = class {
//     constructor(topSpeed) {
//         this.topSpeed = topSpeed;
//     }

//     go() {
//         console.log(`${this.constructor.name} is going at ${this.topSpeed} mph`);
//     }
// }

// const vehicle2 = new Vehicle2(20);
// vehicle2.go();


// // NOTE This method is functionally equivalent to the previous.
// // NOTE This is just like function expressions, this treats our class like a
// //      variable.












// // If you happen to come across code prior to 2009 this is how you might
// // find a class declaration.

// function Vehicle3(topSpeed) {
//     this.topSpeed = topSpeed;

//     this.go = function() {
//         console.log(`${this.constructor.name} is going at ${this.topSpeed} mph`);
//     }
// }

// const vehicle3 = new Vehicle3(30);
// vehicle3.go();

// NOTE This method is functionally equivalent to the previous 2.
// NOTE This function doesn't "return" anything, the function is just being
//      used like a constructor().
// NOTE The ... warning below Vehicle3















// Exercise

// 1. Define a "regular" class that describes anything at all. Include
//        atleast 1 property and 1 method.
// 2. Demonstrate how the same class (with a different name)
//        can be defined using a class expression.
// 3. Demonstrate how the same class (with a different name)
//        can be defined using a constructor function.

























// Extending an existing class and accessing parent properties via prototypes
// The super keyword

class Vehicle {
    constructor(topSpeed) {
        this.topSpeed = topSpeed;
    }

    go() {
        console.log(`${this.constructor.name} is going at ${this.topSpeed} mph`);
    }
}

class Car extends Vehicle {
    constructor(topSpeed, make, model, year) {
        super(topSpeed);

        this.make = make;
        this.model = model;
        this.year = year;
        this.lightsOn = false;
    }

    toggleLights() {
        this.lightsOn = !this.lightsOn;
        const lightStatus = this.lightsOn ? "on" : "off";
        console.log(
            `${this.constructor.name} is turning the lights ${lightStatus}`
        );
    }
}

const vehicle = new Vehicle(20);
const car = new Car(100, "Ford", "Focus", 2000);

vehicle.go();
car.go();

car.toggleLights();
console.log(
    `This car is a ${car.year} ${car.make} ${car.model}`
);


// NOTE the super keyword











// The unpredicable behavior of the `this` object....


// class ClickButton {
//     constructor(buttonName) {
//         this.jsOutput = document.querySelector('#jsOutput');
//         this.buttonElement = document.createElement('button');
//         this.buttonElement.innerHTML = buttonName;
//         this.jsOutput.appendChild(this.buttonElement);

//         // Question... when I click this button... what object will be
//         // displayed in the console log?
//         this.buttonElement.addEventListener(
//             'click',
//             () => console.log(this)
//         )
//     }
// }

// const button = new ClickButton("My Button");

// NOTE The meaning of "this" changes inside of callback functions to refer to
//      the object (or function) that the callback is attached to.  Such as
//      the button element.





















// Homework

// 1. Define a Person class with various properties (name, age, etc)
// 2. Add a greet() method to the greet the "Person" by name (console.log the greeting or output to html)
// 3. Create another class, named for a profession, that extends the Person class.  e.g Teacher, Doctor, etc...
// 4. Create a contstructor for your profession and have it add additional properties to the object; Have it call super().
// 5. Add an additional method (eg. for Teacher you might have a "teach()" method that displays a fun fact)
// 6. Create instances of both and demonstrate accessing properties and methods of each











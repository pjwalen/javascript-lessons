
















// Loops are a way of repeating a block of code until some condition(s) are met.

// Examples:
//
//      You have an array that contains all of your sales figures for the day and
//      you need to step through each element in the array to add it to the total.

//      Maybe you want to find all of the numbers that are divisible by 12 between
//      1 and 1000

// JavaScipt includes a number of loops depending on your needs.

// while
// do-while
// for
// for-in 
// for-of











































// The while-loop is used when you have a block of code you wish to run zero or 
// more times depending on the result of a conditional statement.

// While-loops are best suited to tasks where you don't know how many times you
// need to iterate before the end but aren't restricted to those.

// Syntax for a while-loop

/*
    while (<conditional statement>) 
    {

        <the block of code to run while the conditional statement is true>;

    }
*/



// NOTE This is eerily similar to an if statement.  In fact you can think of
//        the while loop as being like an if statement that continues in a loop 
//        while the condition remains true.



























// Example while loop


// // Find all of the multiples of 7 between 0 and max.
// function calculate_multiples_of_7(max) {

//     // Initialize an array named multiples to store the list of multiples
//     // we're trying to calculate.
//     const multiples = [];

//     // Declare a number outside of the scope of our while-loop so we can track
//     // where we left off between iterations of the loop
//     let currentMultiple = 0;

//     // Loop until currentNumber is greater than 200.
//     while (currentMultiple < max) {

//         // Add 7 to the currentNumber and assign it back to currentNumber
//         currentMultiple = currentMultiple + 7;

//         // Append currentNumber to the multiples array
//         multiples.push(currentMultiple);

//         // Log out the currentMultiple so we can watch as the loop runs
//         console.log(`Adding ${currentMultiple} to the array.`);

//     }

//     // Once we complete this loop, all of the multiples of 7 between 0 and 200
//     // should be in the array.  Let's display our work.
//     return multiples;

// }


// // Find all of the multiples of 7 between 0 and 200
// const multiples_of_7 = calculate_multiples_of_7(200)
// console.log(multiples_of_7);



























// Infinite loops....

// There are times when you need to use an infinite loop, but it's almost
// always an unintended bug when we're in our web browser.



// function causeAnInfiniteLoop() {
//     while (true) {
//         console.log("logging forever.....");
//     }
// }

// causeAnInfiniteLoop();




// NOTE discuss the eventloop, how it is an infinite-loop itself but can't handle when we create an infinite loop inside of it.
// NOTE discuss where an infinite loop might be appropriate.  (eventloops, servers, background processes)






























// Exercise - Add up all the sales figures in a list...

// 1. Declare an array of numbers named "sales_figures" and populate it with
//        some random numbers

// 2. Declare a number called total and set it to 0.

// 3. Create a while loop that runs while the number of elements in the 
//       sales_figures array is greater than 0

// 4. In the body of the while-loop sales_figures.pop() the last element off the end of the 
//       array and add it to the total variable declared earlier.

// 5. After the while-loop ends use console.log() to output the sales totals.

// 6. Can you think of a way to re-write this so you could calculate the
//       total without removing any items from the sales_figure array?



/*
const array = [gfdsgfdsgfds];
let total = 0;
let index = 0;

while (<condition>) {
    element = array[index];
    index ++;
}

console.log(total)
*/


































// A while loop that contains a break statement

//    What if our program is in the process of looping and we need to exit the loop suddenly?



// function do_we_attend(party_guest_list, ex) {
//     // The current element in the party_guest_array that we're checking
//     let index = 0

//     // We haven't seen out ex in the list yet, so we assume we're attending
//     // the party
//     let attending = true;

//     // Step through each guest in the array and determine if they are our ex.
//     while (index < party_guest_list.length) {

//         // If the current guest is our ex, set attending to false and
//         // break.
//         if(party_guest_list[index] === ex) {
//             attending = false;
//             break;
//         }

//         // Assuming this guest isn't our ex, move on to the next guest
//         index = index + 1;
//     }

//     return attending
// }


// const ex = "Karen";
// const party_guest_list = ["Darren", "Cody", "Linda", "Terry", "Jo", "Sam"];
// console.log('Are we attending this party? ', do_we_attend(party_guest_list, ex))




// NOTE This is a contrived example.  If you want to know if an array contains an item, use array.contains() instead.









// Exercise ...


// 1. Create a function with two parameters.  The first is an array of
//        element, and the second is a number or a string that *might* 
//        appear in the array.

// 2. Inside create a while-loop that runs while there are elements
//        remaining in the array (while the length of the array is
//        greater-than 0)

// 3. In the loop body use .shift() to remove the first element of 
//        the array and assign it to a variable.

// 4. Using an if block, check if the variable is equal to the second
//        function parameter (argument).  If it is, break.

// 5. If the the variable doesn't match, let the loop run again.

// NOTE This function doesn't need to return anything.

// 5. Call the function block with two parameters, an array and a second 
// parameter to test / break.

// 6. After the function is called, console.log() the array to the console.

// 7. If everything works, you will either see an empty array, or an array
//      that starts with element immediately proceeding key parameter and 
//      whatever remains after it.


/*

    function someFunction(ourArray, key) {
        while (ourArray.length > 0) {

        }
    }

    const array = [gfdsgfdgfds];
    const key = 320;

    someFunction(array, key);
    console.log(array);

*/










































// The continue statement

// The continue statement can be used in a while loop to cease processing 
// the current iteration through the loop while while still continuing on
// to the next iteration

























// Take in a array of potential guests and the name of an ex, loop through
// the potential_guest_list checking if my ex is present, if they are
// politely forget to add them to the final guest list.



function create_guest_list(potential_guests, ex="Karen") {

    const guest_list = [];
    let index = 0;

    // Step through the potential_guests array one at a time using our 
    // index variable.
    while (index < potential_guests.length) {

        // If the our ex appears in the array at [index] increment index 
        // and continue with the loop.
        if (potential_guests[index] === ex) {
            index ++;
            continue;
        }

        // Add the guest at the current index to the official guest_list.
        guest_list.push(potential_guests[index]);

        // Increment the index and move on to the next iteration.
        index ++;
    }

    // Return the final/official guest list.
    return guest_list;
}

const people_i_know = ["Darren", "Cody", "Karen", "Linda", "Terry", "Jo", "Sam", "Karen", "Karen"];

const guest_list = create_guest_list(people_i_know);

console.log(guest_list);






































// Exercise ...


// 1. Define a function with 2 parameters an array, and a primitive variable.

// 2. Using the previously demonstrated "index" method, step through each item
//      in the array checking to see if it equals the primitive variable that
//      we declared as the second parameter of the function.

// 3. If the value of the element located at index equals the second parameter
//       increment index and continue.

// 4. If the value of the element located at index doesn't equal the second
//       parameter's console.log() the value of the parameter to the console.

 // 5. From outside of the function, call the function and inspect the output





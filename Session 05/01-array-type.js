
// What is an array.....
//
//      Arrays are a collection for storing multiple items (numbers, string,
//      booleans, other arrays etc) under a single variable.
//
//      Note: Arrays can contain multiple types of data at the same time
//      Note: Items in an array are ordered and that is one of the biggest 
//             features to consider when deciding if an array is the right
//             "container" for your collection of data.
//      Note: Arrays are mutable
//















// // How to declare an empty array
// const emptyArray = [];
// console.log(emptyArray);

// // How to declare and initialize it with values
// const arrayOfDogs = ['Corgi', 'Boston Terrier', 'Dachsund'];
// console.log(arrayOfDogs);















// // Accessing elements of an array using an index.  

// const jsOutput = document.querySelector('#jsOutput');
// const arrayOfCats = ['American Shorthair', 'Russian Blue', 'Chartreaux', 'Mainecoon', 'Siamese'];
// // jsOutput.innerHTML = arrayOfCats[100];

// jsOutput.innerHTML = `
//     <h2>This is a list of cat breeds.</h2>
//     <ol>
//         <li>${arrayOfCats[0]}</li>
//         <li>${arrayOfCats[1]}</li>
//         <li>${arrayOfCats[2]}</li>
//         <li>${arrayOfCats[3]}</li>
//         <li>${arrayOfCats[4]}</li>
//     </ol>
// `;

// // NOTE: zero based counting
// // NOTE: what happens if you try to access an index beyond the end of the array?











// // Assigning elements of an array using an index

// const jsOutput = document.querySelector('#jsOutput');
// const arr = [];

// arr[0] = 1.12345
// arr[1] = true
// arr[2] = 'This is the 3rd member of the array, even if you call me [2]'

// jsOutput.innerHTML = `
//     <h2>This is a list of differently typed variables.</h2>
//     <ol>
//         <li>${arr[0]}</li>
//         <li>${arr[1]}</li>
//         <li>${arr[2]}</li>
//     </ol>
// `;

// // Note: WTF?!  I thought const variables couldn't be changed after they are initialized!
// // NOTE: What happens when you try to assign a variable beyond the end of the array?
// // NOTE: What happens when you try to access variables in the empty space?















// Exercise: 

// Write a JS/HTML script that declares and initialized an array of anything.

// Using what we learned about assigning values to an array using indicies add
// 2-3 additional items to the end of the array.

// Display the array in HTML as an ordered-list
//
// <ol>
//    <li>item-1</li>
//    <li>item-2</li>
//    <li>item-3</li>
// </ol>















// // The length property
// const jsOutput = document.querySelector('#jsOutput');
// const apples = ['Braeburn', 'Honeycrisp', 'Grannie Smith'];

// jsOutput.innerHTML = `
//     <h2>This is a list of ${apples.length} apples</h2>

//     <ol>
//         <li>${apples[0]}</li>
//         <li>${apples[1]}</li>
//         <li>${apples[2]}</li>
//         <li>${apples[3]}</li>
//     </ol>
// `;

// // NOTE: Adding an item to the end of the array using the length property
















// // Discuss arrays being mutable and what that means.  i.e. When using the assignment operator we are 
// // aliasing and not copying the values.

// const arr1 = ['item-0', 'item-1', 'item-2'];
// const arr2 = arr1
// arr2[3] = 'item-3'
// console.log(arr1);

// // Question: What is the state of the arr1 array when it is printed out to the console?



















// Array methods:
//
// pop(), push(), shift(), unshift(), sort(), reverse(), slice(), join(), concat()

const fruits = ['apple', 'pear', 'grape', 'mango', 'pineapple', 'peach'];
// const anotherList = ['banana', 'cranberry'];
// retval = fruits.concat(anotherList);
// console.log(retval);
// console.log(fruits);

fruits[2] = 'cranberry';
console.log(fruits);


// Exercise

// Write a JS/HTML script that declares and initializes an array of any 5 things.

// Reverse the order of the array

// After reversing, remove the first item in the array 

// Then append it to the end of the array

// Update the middle item of the array with a completely different value not originally included in the list

// Output the entire array in the HTML document as an ordered list



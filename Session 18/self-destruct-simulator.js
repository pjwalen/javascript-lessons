export default class SelfDestructSimulator {
    constructor() {
        this.count = 10;

        this.elements = document.createElement("div");
        this.#configureArmButton();
        this.#configureDisarmButton();
        this.#configureStatus();

    }

    #configureArmButton() {
        this.armButton = document.createElement("button");
        this.armButton.innerHTML = "Arm";
        this.armButton.addEventListener("click", event => this.arm());
        this.elements.appendChild(this.armButton);
    }

    #configureDisarmButton() {
        this.disarmButton = document.createElement("button");
        this.disarmButton.innerHTML = "Disarm";
        this.disarmButton.disabled = true;
        this.disarmButton.addEventListener("click", event => this.disarm());
        this.elements.appendChild(this.disarmButton);
    }

    #configureStatus() {
        this.status = document.createElement("div");
        this.elements.appendChild(this.status);
        this.disarm();
    }

    arm() {
        this.intervalId = setInterval(() => this.tick(), 1000);
        this.status.innerHTML = `Self destruct in ${this.count} second.`
        this.status.className = "armed"
        this.armButton.disabled = true;
        this.disarmButton.disabled = false;
    }

    tick() {
        this.status.innerHTML = `Self destruct in ${--this.count} second.`;
        if(this.count == 0) {
            this.detonate();
        }
    }

    detonate() {
        this.disarmButton.disabled = true;
        clearInterval(this.intervalId);
        this.status.innerHTML = "Self-destruct has detonated."
        this.status.className = "detonated"
    }

    disarm() {
        this.status.innerHTML = `Self destruct has been disarmed with ${this.count} seconds remaining.`
        this.status.className = "disarmed"
        this.armButton.disabled = false;
        this.disarmButton.disabled = true;
        clearInterval(this.intervalId);
    }
}
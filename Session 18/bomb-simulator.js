export default class BombSimulator {
    constructor() {
        this.elements = document.createElement("div");
        this.#configureArmButton();
        this.#configureDisarmButton();
        this.#configureStatus();
    }

    #configureArmButton() {
        this.armButton = document.createElement("button");
        this.armButton.innerHTML = "Arm";
        this.armButton.addEventListener("click", event => this.arm());
        this.elements.appendChild(this.armButton);
    }

    #configureDisarmButton() {
        this.disarmButton = document.createElement("button");
        this.disarmButton.innerHTML = "Disarm";
        this.disarmButton.disabled = true;
        this.disarmButton.addEventListener("click", event => this.disarm());
        this.elements.appendChild(this.disarmButton);
    }

    #configureStatus() {
        this.status = document.createElement("div");
        this.elements.appendChild(this.status);
        this.disarm();
    }

    arm() {
        this.timeoutId = setTimeout(() => this.detonate(), 5000);
        this.status.innerHTML = "The bomb is armed and will explode in 5 seconds."
        this.status.className = "armed"
        this.armButton.disabled = true;
        this.disarmButton.disabled = false;
    }

    detonate() {
        this.disarmButton.disabled = true;
        this.status.innerHTML = "The bomb has detonated."
        this.status.className = "detonated"
    }

    disarm() {
        this.status.innerHTML = "The bomb is disarmed."
        this.status.className = "disarmed"
        this.armButton.disabled = false;
        this.disarmButton.disabled = true;
        clearTimeout(this.timeoutId);
    }
}
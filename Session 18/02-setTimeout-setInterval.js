















/*
    setTimeout()

    Is a global function that executes a block of code after a timeout
    expires.


*/


















// // Basic setTimeout usage
// // timeoutId = setTimeout(<function>, <milliseconds>)

// const jsOutput = document.querySelector("#jsOutput");
// const timeoutId = setTimeout(
//     () => jsOutput.innerHTML = "Hello world!",
//     5000
// )

// // Clearing a timeout
// // console.log("I'm clearing the timer so it doesn't execute");
// // clearTimeout(timeoutId);



























// // A more fun example....
// import BombSimulator from "./bomb-simulator.js";
// const bombSimulator = new BombSimulator();
// const jsOutput = document.querySelector("#jsOutput");

// jsOutput.appendChild(bombSimulator.elements);
















/*
    setInterval()

    Is a global function that executes a block of code, repeatedly, after at a
    given interval until it is cancelled.

*/





















// Basic setInterval usage
// const jsOutput = document.querySelector("#jsOutput");
// let count = 0;
// const intervalId = setInterval(() => jsOutput.innerHTML = count++, 1000);


// // Clearing an interval
// setTimeout(() => clearInterval(intervalId), 5000);






// import SelfDestructSimulator from "./self-destruct-simulator.js";
// const selfDestructSimulator = new SelfDestructSimulator();
// const jsOutput = document.querySelector("#jsOutput");

// jsOutput.appendChild(selfDestructSimulator.elements);



















/*
    Final Project


    Design a web application that combines multiple "widgets" into a single cohesive app.

    - This application needs at least 2 (but certainly more is better) widgets
        that interact with eachother.  E.g. A task list and a progress bar that
        grows and shrinks as tasks are completed.
             https://developer.mozilla.org/en-US/docs/Web/HTML/Element/progress
    - The application should define multiple classes (one for each widget and
        others as needed).
    - At least one class should have some publicly available methods and/or
        properties that can be called or modified by other components in
        the page.
    - Your application should make use of at least 2 styles of loops
        (while/for/do).  If there isn't a logical reason to use 2 styles of
        loops, contrive something.
    - Your application should make use of setInterval or setTimeout
    - Your application should be set as type module and should import classes
        and code from other javascript modules.
    - Your application should make use of className or classList to update CSS
        as elements change.
    

*/



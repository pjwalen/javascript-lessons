class MessageBoard {
    constructor() {
        this.elements = document.createElement("div");

        this.#configureInput();     // Create this.input
        this.#configureButton();    // Create this.button
        this.#configureUL();        // Create this.ul
    }

    #configureInput() {
        // Configure the text input
        this.input = document.createElement("input");
        this.input.setAttribute("type", "text");
        this.elements.appendChild(this.input);
    }

    #configureButton() {
        // Configure the button
        this.button = document.createElement("button");
        this.button.innerHTML = "Add Item";
        this.button.addEventListener("click", event => this.#handleClick());
        this.elements.appendChild(this.button);
    }

    #configureUL() {
        // Configure the unordered-list
        this.ul = document.createElement("ul");
        this.elements.appendChild(this.ul);
    }

    #handleClick(event) {
        this.addItem(this.input.value);
        this.input.value = "";
    }

    addItem(item) {
        const li = document.createElement("li");
        li.innerHTML = item;
        this.ul.appendChild(li);
    }
}

function addItem(taskList, item) {
    console.log(`Adding ${item}.`)
    taskList.addItem(item);
}













/*



    Imports and Exports

    Imports are used to import "exported" bindings from another JavaScript
    module/file.
    
    The exported bindings are read-only to the importing module, but can be
    updated from the exporting module.



*/



















// Exporting/importing a default binding - Open messageboard.js and
//      demonsrtrate the export default expression


// import MessageBoard from './messageboard.js';

// const messageBoard = new MessageBoard();
// const jsOutput = document.querySelector("#jsOutput");
// jsOutput.appendChild(messageBoard.elements);
// messageBoard.addItem("Orange");





// NOTE Demonstrate declaring the default export at the bottom of the file
// NOTE Demonstrate declaring the default export inline
// NOTE Demonstrate renaming the imported default binding





















// Exporting/importing a non-default binding - Open messageboard.js and
//      demonsrtrate the export expression

// import MessageBoard from "./messageboard.js"
// import { addItem } from "./messageboard.js";

// const messageBoard = new MessageBoard();
// const jsOutput = document.querySelector("#jsOutput");
// jsOutput.appendChild(messageBoard.elements);


// addItem(messageBoard, "Orange");


// NOTE Demonstrate declaring the export at the bottom of the file
// NOTE Demonstrate declaring the export inline
// NOTE Demonstrate renaming the imported binding
// NOTE Demonstrate importing default and non-default on a single-line















// Exercise, try creating a javascript module and exporting/importing various
//    classes and functions.  Attempt to create 1 default export and at least
//    1 non-default export


// Question, can you export/import a regular variable and if so, can you
// re-assign the variable from the module where it was it was imported from?






















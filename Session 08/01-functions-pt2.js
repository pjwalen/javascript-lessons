// Variable scope
// Declaring a variable using var, let, const
// Global Variables




















// // A variables scope determines which blocks of javascript code
// // can interact with the variable while the code is executing.  

// // For instance, if we declare a variable outside of a function
// // and subsequently call the function, the variable declared outside
// // of the function can be said to be in-scope.

// var myVariable = 'Any code that can see me has me in-scope.';

// function logVariable() {
//     const jsOutput = document.querySelector('#jsOutput');
//     jsOutput.innerHTML = myVariable;
// }

// logVariable();


// // NOTE Since myVariable is declared outside of a function it is said to be
// //        in the global scope.  It is good practice not to declare anything
// //        in the global scope.

// // NOTE This behavior is the same regardless of const, let and var























// In contrast to this, if we declare a variable inside of a function
// and call the function, the variable declared inside of the function
// can be said to be out-of-scope to code writen outside of the function.

// function setVariable() {
//     const myVariable = 'Any code that can see me, has me in-scope.';
// }

// setVariable();
// const jsOutput = document.querySelector('#jsOutput');
// jsOutput.innerHTML = myVariable;




// // NOTE Check the javascript console for an error.

// // NOTE Since the myVariable variable is declared within the setVariable
// //    function, it is said to be in the local scope of the setVariable
// //    function.

// // NOTE This behavior is the same regardless of const, let and var












// When we declare a variable using either the let or const keywords we are 
// declaring that variable to be in-scope inside of whichever block it occurs
// along with any blocks that the source block contains...


// function testLetAndConstScope1() {
//     // Notice we are declaring this variable in the function, but outside
//     // of the if block.
//     let varA = 'This is varA';
//     if(true) {
//         console.log('Inside the if block: ' + varA);
//     }
//     console.log('Outside of the if block: ' + varA);
// }

// testLetAndConstScope1();



// function testLetAndConstScope2() {
//     // Notice we are declaring this variable in the if statement 
//     // and not directly in the base function block.
//     if(true) {
//         var varA = 'This is varA';
//         console.log('Inside the if block: ' + varA);
//     }
//     console.log('Outside of the if block: ' + varA);
// }

// testLetAndConstScope2();



// // NOTE Swap the 'let' statements with 'const' statements to show that both
// //        operate the same with respect to scoping.























// The var keyword

// When we use the var keyword inside of a function, the variable is in-scope
// for all code-blocks contained in the function after the variable has been 
// declared.

// function testVarScope() {
//     if (true) {
//         var varA = 'This is varA';
//     }

//     // Notice this is being called from outside of the if block.
//     console.log('Outside of the if block: ' + varA);
// }

// function testVarScope1() {
//     var varA = "This is another varA"
//     console.log(varA);
// }

// console.log(varA);

// testVarScope();
// testVarScope1();







// Exercise... Break ALL THE THINGS!

// We spend so much time writing code the "right" way, but sometimes we can
// learn more by intentionally doing things the wrong way, to see how they
// break.

// Write a function to demonstrate how each of the following errors occurs.

// 1. Demonstrate the error that occurs when we try to access a const or let 
//      variable outside of the block it was declared.

// 2. Demonstrate the error that occurs when we try to access a var 
//      variable outside of the function it was declared.

// 3. Demonstrate the error that occurs when we try to access a const/let
//      variable inside of a function, but which wasn't declared inside of
//      the block our scope requires.

// 4. Explain the reason you should't really use variables in the global
//      scope.
















// Callback functions are a function passed into another function as an argument, 
// which is then invoked inside the outer function to complete some kind of routine 
// or action.


// // This is a function that expects a callback function as one of its arguments
// function doSomething(functionToCall) {
//     const stringOfText = "This argument is provided by the doSomething function.";

//     functionToCall(stringOfText);

// }

// // This is the function we're going to pass in as a callback
// const ourCallbackFunction = function(s) {
//     console.log(s);
//     return
// }

// // Calling doSomething() with our callback function.
// doSomething(   ourCallbackFunction    );
















// A slightly more practical example...

const jsOutput = document.querySelector('#jsOutput');
jsOutput.innerHTML = '<ul></ul>';

function displayProfits(salesPerson) {
    const jsOutputList = document.querySelector('#jsOutput > ul');
    jsOutputList.innerHTML += `<li>${salesPerson.name} $${salesPerson.profit}.00</li>`;
}

const profitPerSalesPerson = [
    {name: "Stephen", profit: 10.00},
    {name: "Gerald", profit: 100.00},
    {name: "Angela", profit: 20.00},
    {name: "Ryan", profit: 55.00},
    {name: "Kim", profit: 120.00},
    {name: "Terry", profit: 3.00},
];

profitPerSalesPerson.forEach(displayProfits);



// NOTE Adding additional list members doesn't require knowing the total length of the list when
//        using the foreach method with a callback.




// Exercise...

// Author a list of objects and a callback function to structure 













// Arrow functions are a "compact" alternative to traditional functions
    // Arrow functions with 1 parameter
    // Arrow functions with 0 or multiple parameters
    // Arrow functions and returning data with return
    // Arrow functions and returning data as a single line expression

// Exercise... read this regular function declaration and turn it into the equivalent arrow function


// Array.forEach()

// const jsOutput = document.querySelector("#jsOutput");
// const div = document.createElement("div");
// const formTemplate = document.querySelector("#formTemplate");

// div.innerHTML = `
//     <h1>Hello world!</h1>
// `;
// div.classList.add('test1', 'test2', 'test3');
// jsOutput.appendChild(div);
// const form = formTemplate.content.firstElementChild.cloneNode(true);
// jsOutput.appendChild(form);
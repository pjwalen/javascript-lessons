
















// What is an object.....
//
// Objects are a collection for storing multiple items under a single variable, similar to arrays.
// Objects are different from arrays in the way its items (aka properties) are declared and how 
// they are accessed.  When accessing array items, you refer to it's index; it's position in the 
// array.  When accessing a property of an object, you refer to it by it's property name.

// NOTE: This means you can think of objects as being unordered and arrays as being ordered.





















// How to declare an empty object
// const myObject = {};
// const anotherObject = new Object();


// // How to declare and initialize it with values
// const aPersonObject = {
//     firstname: "Patrick",
//     lastname: "Walentiny",
//     age: 42,
//     totallyRocks: true
// };

// console.log(aPersonObject);























// // // Accessing properties of an object by name using [] notiation (bracket)
// // // Accessing properties of an object by name using . notiation (dot)
// const jsOutput = document.querySelector('#jsOutput');

// const car = {
//     make: "Honda",
//     model: "Civic",
//     trim: "Si",
//     year: 2013,
//     color: "black",
//     'owner description': 'We are the original owners and are looking to upgrade to our next chariat!'
// };

// console.log(car.make);
// console.log(car.model);
// console.log(car.trim);
// console.log(car['year']);
// console.log(car.year);
// console.log(car['owner description']);

// jsOutput.innerHTML = `
//     <h2>Car for sale!</h2>
//     <p>
//         ${car['owner description']}
//         <table>
//             <tr>
//                 <td>Make</td><td>${car.make}</td>
//             </tr>
//             <tr>
//                 <td>Model</td><td>${car.model}</td>
//             </tr>
//             <tr>
//                 <td>Trim</td><td>${car.trim}</td>
//             </tr>
//             <tr>
//                 <td>Year</td><td>${car.year}</td>
//             </tr>
//         </table>
//     </p>
// `;

// // // NOTE: Explain when you might need to use the [] notation
// // // NOTE: Explain that they can use both single and double-quotes using bracket notation






















// // Assigning additional parameters to an object using [] notation
// // Assigning additional parameters to an object using . notation
// const jsOutput = document.querySelector('#jsOutput');

// const customer = {};  // Remember, this is the same as const customer = {};
// customer.firstname = "Steve";
// customer.lastname = "Wellington";
// customer['mobile phone'] = "(612) 555-0485"

// console.log(customer);

// jsOutput.innerHTML = `
//     <h2>Customer Record</h2>
//     <ul>
//         <li><strong>Name</strong> - ${customer.firstname} ${customer.lastname}</li>
//         <li><strong>Mobile Phone</strong> - ${customer['mobile phone']}</li>
//     </ul>
// `;















// // Demonstrate the 'in' operator for objects
// const secretLocation = {
//     lat: 44.94698085577772,
//     lon: -93.10828814987083
// };

// if ('lon' in secretLocation) {
//     console.log(`There is a latitude in this object and it is: ${secretLocation.lat}`);
// }

// // Demonstrate the Object.keys() method
// console.log(Object.keys(secretLocation));

















// Exercise...
//
// 1. Create an html/js script combo
// 2. Declare and initialize an object (be creative).
// 3. Add/assign an additional property to the object using dot notation.
// 4. Add/assign an additional property using bracket notation
// 5. Output your object to HTML structuring your HTML however you see fit (feel free to steal my examples) 

// 6. Extra points: At the end of your script, write a conditional block (if statement) that tests if a property 
//        exists in the object, if it does, output the properties value to the console.




























// // Demonstrate how objects, and arrays can be nested in eachother to make even more complex datatypes

const customer = {
    "firstname": "Myrtle",
    "lastname": "Bigglesworth",
    "telephone": [
        {type: "office", number: "(612) 555-0486"},
        {type: "office", number: "(612) 555-0487"},
        {type: "home", number: "(612) 555-0488"},
        {type: "home", number: "(612) 555-0489"},
    ]
};

console.log('The customer object: ', customer);
console.log('The firstname property of the customer object: ', customer.firstname);
console.log('The lastname property of the customer object: ', customer.lastname);
console.log('The telephone property of the customer object: ', customer.telephone);
console.log('The first item in the telephone property of the customer object: ', customer.telephone[0]);
console.log('The type property of the first item in the telephone property of the customer object: ', customer.telephone[0].type);
console.log('The number property of the first item in the number property of the customer object: ', customer.telephone[0].number);

const jsOutput = document.querySelector('#jsOutput');
jsOutput.innerHTML = `
    <h2>Customer Record</h2>
    <ul>
        <li><strong>Name</strong> - ${customer.lastname}, ${customer.firstname}</li>
        <li>
            <strong>Telephone</strong> 
            <ol>
                <li><strong>${customer.telephone[0].type}</strong> - ${customer.telephone[0].number}</li>

                <li><strong>${customer.telephone[1].type}</strong> - ${customer.telephone[0].number}</li>
                
                <li><strong>${customer.telephone[2].type}</strong> - ${customer.telephone[0].number}</li>
            </ol>
        </li>
    </ul>
`;


// // NOTE:  This is how *most* APIs structure the data they send you when you call them, so learning
// //           to read and understand these datastructures is critical for what you want to accomplish.
// // NOTE: https://public-api.gohighlevel.com/






// Homework...

// Design a complex object of your own that includes nested lists and object (similar to the above)
//
// In HTML layout your complex object similar to the example above where all of the data for the
// object is displayed in nested ordered-list (for arrays) and unordered-lists (for objects).

// Lookup a youtube video, blog, or mdn link on the topic of JSON (JavaScript Object Notation)








// What is a function...
//
//   A function is a repeatable block of code that can be "called" upon 
// elsewhere in your javascript code.
//
//   Functions can take arguments that allow us to pass variables into
// our functions when we "call" them.
//
// Functions may return a variable when they are called.















// // Function basic declaration.

// function myFunction() {
//     const jsOutput = document.querySelector("#jsOutput");
    
//     // Notice I am using += to append additional string
//     // data each time the functions is called.
//     jsOutput.innerHTML += "Hello world!<br>";
// }


// // Calling our function
// myFunction();
// myFunction();
// myFunction();
// myFunction();
// myFunction();

// console.log('Script ended...');














// // Function expression
// const myFunction = function() {
//     const jsOutput = document.querySelector("#jsOutput");
//     jsOutput.innerHTML += "Hello world!<br>";
// };

// myFunction();
// myFunction();
// myFunction();
// myFunction();
// myFunction();
// myFunction();

// NOTE Notice the semi-colon at the end of the expression
// NOTE Function expressions are the newer/more modern way to declare a function
// NOTE There is nearly no difference between normal functions and function expressions
// NOTE The only difference is hoisting....













// Hoisting functions


// hoistingDemo();

// function hoistingDemo() {
//     console.log("Can we see this output?");
// }

// const hoistingDemo = function() {
//     console.log("Can we still see this output?");
// }

// NOTE When declaring a function, it doesn't matter where the function is declared
//  in the file, but when using function expressions it does.

// NOTE Some developers prefer prefer function expressions because it enforces "good"
//   structuring of your code, some prefer functions so they can appear "out of the way"
//   of their important code.  What you choose depends on what you find easiest to read.










// Exercise..
// 
// 1. Create a JS and HTML file and link them togther
// 2. In the HTML file, create 2 <div>'s inside your <body> block and give
//      each a different id.  i.e. id="jsOutput1" and id="jsOutput2"
// 3. Declare a function that when called will assign a string to the 
//      innerHTML property of the first <div>.
// 4. Create a different function using a function expression
//      that when called will assign a string to the innerHTML
//      property.
// 5. After declaring both functions, call each.
















// // Function parameters


// // Calling a function with parameters
// const functionWithParameters = function(name) {
//     const jsOutput = document.querySelector("#jsOutput");
//     jsOutput.innerHTML = `Hello ${name}!`;
// }

// functionWithParameters("Patrick");

// NOTE demonstrate re-writing the above as a function expression
// NOTE demonstrate re-writing the above with an additional parameter
// NOTE demonstrate what happens if we don't add arguments to our function call













// Exercise...
//
// Re-write your code from the first exercise so that there is only 1 function that takes 2 parameters.

// The first parameter we will pass to document.querySelector(<parameter>), so that
// when we call the function we can tell it which of the 2 divs we wish to populate.

// The second parameter will be assigned to the innerHTML of the selected div. So we can 
// pass a custom message to our div when we call the function.

// Once done, call this function twice, once for each of the 2 divs in your HTML file, each with a unique message.













// // Returning a variable



// function double(n) {
//     const double_n = n * 2;
//     return double_n;
// }

// const jsOutput = document.querySelector('#jsOutput');
// const result = double(32);
// jsOutput.innerHTML = `If you double 32 the result is ${result}`



// // NOTE Demonstrate what the double function looks like if we put the expression
// //       after the return statement
// // NOTE Demonstrate calling the double() function from inside the string template literal.
// // NOTE Demonstrate what happens when you try to assign the output of a function
// //        that doesn't return a variable.













// Default parameters



function lookupStateAbbrev(state = "Delaware") {

    // This is just an objects whose properties are all the capitalized
    //  spelling of each state or territory.
    const statesToAbbrev = {
        Alabama: "AL", Kentucky: "KY", Ohio: "OH", Alaska: "AK",
        Louisiana: "LA", Oklahoma: "OK", Arizona: "AZ", Maine: "ME",
        Oregon: "OR", Arkansas: "AR", Maryland: "MD", Pennsylvania: "PA",
        "American Samoa": "AS", Massachusetts: "MA", "Puerto Rico": "PR", California: "CA",
        Michigan: "MI", "Rhode Island":	"RI", Colorado: "CO", Minnesota: "MN",
        "South Carolina": "SC", Connecticut: "CT", Mississippi: "MS", "South Dakota": "SD",
        Delaware: "DE", Missouri: "MO", Tennessee: "TN", "District of Columbia": "DC",
        Montana: "MT", Texas: "TX", Florida: "FL", Nebraska: "NE",
        "Trust Territories": "TT", Georgia: "GA", Nevada: "NV", Utah: "UT",
        Guam: "GU", "New Hampshire": "NH", Vermont: "VT", Hawaii: "HI",
        "New Jersey": "NJ", Virginia: "VA", Idaho: "ID", "New Mexico": "NM",
        "Virgin Islands": "VI", Illinois: "IL", "New York": "NY", Washington: "WA",
        Indiana: "IN", "North Carolina": "NC", "West Virginia": "WV", Iowa: "IA",
        "North Dakota": "ND", Wisconsin: "WI", Kansas: "KS", "Northern Mariana Islands": "MP",
        Wyoming: "WY",
    };

    return statesToAbbrev[state];
}


const jsOutput = document.querySelector("#jsOutput");
const stateAbbrev = lookupStateAbbrev();
jsOutput.innerHTML = `The state abbreviation is: ${stateAbbrev}`

// NOTE Demonstrate what happens if we provide an argument to lookupStateAbrev()






// Exercise or homework....

// Modify our second exercise so that the "css selector" parameter has a default
//  that causes the function to write its output to the first div, if one is not
//  specified when the function is called.

// Don't add a default for the string we output.

// Test out the function by calling it with and without the css selector 
// parameter/argument.

// Self-study - Research the topics
//
//     - "JavaScript callback functions"
//     - "JavaScript arrow functions" 
//
// Write some test code to test your assumptions about these topics
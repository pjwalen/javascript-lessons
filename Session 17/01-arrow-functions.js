













/*
    Arrow Functions

    An arrow function expression is a compact alternative to a traditional
    function expression, with some semantic differences and deliberate
    limitations in usage:

        - Arrow functions don't have their own bindings to this, arguments, or
            super, and should not be used as methods.
        - Arrow functions cannot be used as constructors. Calling them with
            new throws a TypeError. 

*/


















// Define a basic arrow function
// function addNumbers1(a, b) {
//     return a + b;
// }


// const addNumbers = (a, b) => {
//     return a + b;
// };
// const result = addNumbers(10, 5);
// console.log(result);


// const salesFigures = [1, 8, 2, 10, 3, 4, 23];
// const forgedSalesFigures =  salesFigures.map(n => n+1);
// console.log(forgedSalesFigures);






















// Usual  vs Concise bodies

// // Usual body
// const multiplyNumbersUsual = (a, b) => {
//     return a * b;
// };

// // Concise body
// const multiplyNumbersConcise = (a, b) => a * b;



// const result1 = multiplyNumbersUsual(10, 5)
// const result2 = multiplyNumbersConcise(10, 5)
// console.log(result1, result2);


// NOTE The "implied return"
// NOTE Concise bodies only work when the function has one expression.














// Optional parenthesis
// const outputWParens = (output) => {
//     console.log(output);
// };

// const outputWOParens = output => console.log(output);

// outputWParens("With parens");
// outputWOParens("Without parens");


// NOTE these functions can be re-written as a concise arrow function.

















// Exercise

// 1. Write a basic arrow function that takes an input performs a simple
//        calculation and returns the result.
// 2. Create a copy of this function and rename it and then modify it
//        to use a concise body.
// 3. Create a copy of this function and rename it and then modify it
//        so it doesn't include parenthesis around the arguments.
// 4. Call all 3 functions and console.log the results
























// Arrow functions don't have their own "this"

class ItemList {
    constructor() {
        // Configure the parent element
        this.elements = document.createElement("div");

        this.#configureInput();     // Create this.input
        this.#configureButton();    // Create this.button
        this.#configureUL();        // Create this.ul
    }

    #configureInput() {
        // Configure the text input
        this.input = document.createElement("input");
        this.input.setAttribute("type", "text");
        this.elements.appendChild(this.input);
    }

    #configureButton() {
        // Configure the button
        this.button = document.createElement("button");
        this.button.innerHTML = "Add Item";
        this.button.addEventListener("click", event => this.#handleClick());
        this.elements.appendChild(this.button);
    }

    #configureUL() {
        // Configure the unordered-list
        this.ul = document.createElement("ul");
        this.elements.appendChild(this.ul);
    }

    #handleClick(event) {
        this.addItem(this.input.value);
        this.input.value = "";
    }

    addItem(item) {
        const li = document.createElement("li");
        li.innerHTML = item;
        this.ul.appendChild(li);
    }
}

const itemList = new ItemList();
const jsOutput = document.querySelector("#jsOutput");
jsOutput.appendChild(itemList.elements);
itemList.addItem("Apple");
console.log("The end");

// NOTE Use this as a platform for discussing debugging



// HOMEWORK
// 1. Research the following items:document.querySelectorAll()
//         a. Using JavaScript to set an element's CSS class (i.e. Element.className  / Element.classList)
//         b. Research document.querySelectorAll()

// 2. Start thinking about other class/widgets you might be able to make like above. (Examples below)
//      a. A menu class that includes a UL of links and depending on what link
//           is clicked, will determine what's rendered in a div below.
//      b. A button that perform's a fetch and renders the output
//      c. A task list with checkboxes and/or delete functionality
//      d. A customer feedback form that posts fields the console.log() to
//           simulate POSTing data online, then clears the contents
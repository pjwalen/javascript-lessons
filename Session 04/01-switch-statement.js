
// What is the switch statement.....

// You can think of the switch statement similarly to the if/if-else/else statements.  It exists
// to give programmers the ability to conditionally run code.

// The difference between the two can be explained like this.

// Given an if/if-else/else declaration, only one of the blocks (if any) will
// be evaluated.  The block whose boolean expression evaluates to 
// true first is the one selected to run.

// With switch we use a key instead of a boolean statement along with
// a list of "cases".  When Javascript evaluates a switch block, it 
// compares the value of each case with the key and if they are equal the code
// for the matching case is run ALONG WITH ALL SUBSEQUENT CASES.



// switch (<key>) {
//     case <value1>:
//         // Zero or more lines of code to be executed if <key> matches <value1>
//         console.log('Doing something');
//
//     case <value2>:
//         // Zero or more lines of code to be executed if <key> matches <value2> or
//         // if any of the above cases matched.
//         console.log('Doing something');
//
//     case <value3>:
//         // Zero or more lines of code to be executed if <key> matches <value3> or
//         // if any of the above cases matched.
//         console.log('Doing something');
//
//     default:
//          // The (optional) default block always run regardless of the previous cases if present.
//         console.log('Doing something');
// }
















// Here is a simple example of a switch-case block

// const jsOutput = document.querySelector('#jsOutput');
// const membershipLevel = 'platinum';
// let membershipPerks = '';

// switch (membershipLevel) {
//     case 'platinum':
//         membershipPerks = '<li>Private parking spot</li>' + membershipPerks;

//     case 'gold':
//         membershipPerks = '<li>Premium concierge service</li>' + membershipPerks;
    
//     case 'silver':
//         membershipPerks = '<li>10 Free hours with a trainer every month</li>' + membershipPerks;
    
//     default:
//         membershipPerks = '<li>Access to the club</li>' + membershipPerks;
// }

// jsOutput.innerHTML = `
//     <h1>Membership Perks</h1>
//     <ul>
//         ${membershipPerks}
//     </ul>
// `;

// Note: String template literal in use on the last line.
// Note: While we only have one line of code per case in this example, you can add as many lines as 
//         you'd like between cases.
// Note: You don't actually have to have any code at all between cases... demonstrate adding an onyx
//       membership on top of platinum











// Exercise:

// This last discussion introduced two new topics.  Switch-case, and string templates.  Let's 
// try to get some muscle memory with them.

// 1. As we have done before, author and HTML and Javascript file and link them together with the 
//       <script> tag.  Don't forget to add your defer attribute.  

// 2. Just as I did above, create your own club with your own set of levels and ammenities.

// 3. Create a membershipLevel variable and set it to one of the levels.

// 4. Create a membershipPerks variable and set it as an empty string.

// 5. Configure your cases in your switch block, so each level will prepend its perks to the 
//       membershipPerks string.

// 7. Write your list of perks out on the screen for the given level as an unordered-list













// So what if we have a case where we don't want all of the subsequent cases to run?
// Enter the break statement..............


// const jsOutput = document.querySelector('#jsOutput');
// const membershipLevel = 'employee';
// let membershipPerks = '';

// switch (membershipLevel) {
//     case 'employee':
//         membershipPerks = '<li>Limited access to the club on alternate Tuesdays between 2 and 3pm.</li>';
//         break;

//     case 'platinum':
//         membershipPerks = '<li>Private parking spot</li>' + membershipPerks;

//     case 'gold':
//         membershipPerks = '<li>Premium concierge service</li>' + membershipPerks;
    
//     case 'silver':
//         membershipPerks = '<li>10 Free hours with a trainer every month</li>' + membershipPerks;
    
//     default:
//         membershipPerks = '<li>Access to the club</li>' + membershipPerks;
// }

// jsOutput.innerHTML = `
// <h1>Membership Perks</h1>
// <ul>
//     ${membershipPerks}
// </ul>
// `;


// Note: Explain the break keyword
// Note: The break keyword is used in loops as well





// Exercise: 

// 1. Author a javascript and html file and link them together using the <script> tag

// 2. Declare a variable named month and initialize it with the value of one of the  months of the year e.g. let month = 'february';

// 3. Declare a variable named output and leave it uninitialized.  e.g. let output;

// 3. Create a switch/case block with `month` as the key.

// 4. Create a case for each month and configure them such that: 
//      - If the `month` key is equal to a month that occurs in the first half of the year assign
//           `${month} occurs in the first half of the year` to the output variable
//      - If the `month` key is equal to a month that occurs in the latter half of the year assign
//           `${month} occurs in the latter half of the year` to the output variable
//      - Use at-most 2 break statements

// 5. Write the output string to the webpage using a .innerHTML attribute.
 














// Another common use of a switch statement is as a 1-to-1 replacement with the if/if-else/else block.






const percentage = 59;
let letterGrade = null;

switch (true) {
    case percentage >= 90:
        letterGrade = 'A';
        break;
    case percentage >= 80:
        letterGrade = 'B';
        break;
    case percentage >= 70:
        letterGrade = 'C';
        break;
    case percentage >= 60:
        letterGrade = 'D';
        break;
    default:
        letterGrade = 'F';
}

jsOutput.innerHTML = `
<h1>Class Grade</h1>
<p>
    With a score of ${percentage}% you have a letter grade of ${letterGrade}
</p>
`;



// Note: This form is of the switch statement is equivalent to using an if/else-if/else clauses.
// Note: You can actually build any switch statement as a collection of if clauses.
// Ternary Operators

// Ternary operators provide us a simple (single-line) solution for conditionally 
//    assigning a variable.








// const x = <boolean expression> ? <variable_to_assign_if_boolean_is_true> : <variable_to_assign_if_boolean_is_false>





























// const jsOutput = document.querySelector('#jsOutput');
// const canPigsFly = true;

// // The following lines are equivalent....
// let s = canPigsFly ? 'Pigs can file' : "Pigs can't fly";

// // let s;
// // if (canPigsFly) {
// //     s = 'Pigs can fly';
// // } else {
// //     s = 'Pigs can\'t fly'
// // }

// jsOutput.innerHTML = `<p>${s}</p>`


// Note: How the ternary operator is more concise and easy to read, once you know the syntax for it
// Note: This solves for the most common use-case of a conditional with less code

















// // One common use of a ternary operator is to assign a default value if one isn't present
// let userTypedName;
// userTypedName = userTypedName ? userTypedName : 'Default User Name';
// console.log(userTypedName);





















// Exercise
//
//  Below is an if/if-else/else block... read through it and attempt to re-write it using
//  a ternary operator

let city;
let headquarters = 'Wilmington, DE';

// If the variable city is not defined, set city equal to headquarters
if (!city) {
    city = headquarters;
}

console.log(`New location added to the database ${city}`);




// value = boolean ? 'this is true' : 'this is false'; 















/*

    Promises - 

    Promises represent the "eventual completion" of a given function.

    Remember back a few sessions when I demonstrated what an infinite loop
    does to a web browser?  While that loop was running, elements in the web
    browser were unresponsive because my function never completed, never
    allowing the browsers main-loop (main-thread) to take back control.

    When JavaScript function returns a promise to you, what it is saying is:

    "I've started function on it's own track of execution so it won't block
    the browsers main-loop/main-thread, but I don't know when it will
    complete, here take this "promise" object as an I.O.U. When the function
    completes I will update the promise with my results. 


    Let's see this in action.

*/




























function fetchData() {

    fetch("user.json").then(
        // response is the variable that is *REALLY* being returned by the
        // call to fetch()
        function(response) {
            console.log(response.statusText);
        }
    );
}

fetchData();



// NOTE It was written this way to demonstrate the promise object, demonstrate
//      how this is normally written in a more concise way.

// NOTE How this can be even more readable when you use arrow functions.































// Chaining multiple promises together

function fetchDataAndParse1() {
    fetch("user.json").then(
        function(response) {
            const data = response.json()
            console.log("This is what is what response.json() returns: ", data)
            return data;
        }
    ).then(
        function(data) {
            console.log(data);
        }
    );
}

// fetchDataAndParse1();

























// Error handling

function fetchDataAndParse2() {
    fetch("user.json").then(
        function(response) {
            data = response.json()
            console.log("This is what is what response.json() returns: ", data)
            return data;
        }
    ).then(
        function(data) {
            console.log("This is the data that was returned", data);
        }
    ).catch(
        function(error) {
            console.log("Whoopsie, and error occured", error);
        }
    ).finally(
        function() {
            console.log("This happens no matter what.");
        }
    );
}

// fetchDataAndParse2();


// NOTE Demonstrate what happens when the error occurs and doesn't occur, finally exexutes regardless.
// NOTE It doesn't matter if we have multiple .then's in a chain, .catch and .finally will work for all of them.















// How do you create your own promise?

async function calculatePi() {
    console.log("Let's pretend for a moment that this function takes a long time to calculate pi...");
    return 3.14158
}

// console.log(calculatePi());
// calculatePi().then(
//     function(pi) {
//         console.log(pi);
//     }
// );



// NOTE There is another way to do this more *traditionally* using
//      "new Promise()" but there are fewer and fewer reasons to use it these
//      days. This is something we can spend time on later if we need.

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise


















// Hey?!  We've seen async functions before, when we used the await keyword?!

async function fetchDataAndParse3() {

    // Since we are already in an asyncronous function, it doesn't matter if
    // we block execution waiting for a webserver.... so it's a good time to
    // make use of that await keyword!
    const response = await fetch("user.json");
    console.log(await response.json());

}


// fetchDataAndParse3();





// NOTE Why we still need .then() (with .then you can start multiple async
//      functions simultaneously).



















/*
    HOMEWORK REVIEW - Research Promises, async and await

    Answer these questions....

    1. What is a promise and what is it used for?
    2. What does Promise.then(), Promise.catch() and Promise.finally() do?
    3. What does "await" do and how does it related to Promises?
    4. What does "async" do and how does it relate to "await"?

*/


/*

    NEW HOMEWORK - Test promises, async and await

    1. Create a function that fetches data using the .then() method.
    2. Chain a second .then keyword to parse the JSON data that is returned
    3. Add a .catch() method to the chain so you can test-trigger it to see if you can make it work.
    4. Add a .finally() method to the chain and see how it performs.

    5. Rewrite the above steps as an async function.

*/


function homework() {
    fetch("user.json").then(
        // Use fetch() to get a json file
        // parse the response with response.json()
        // return the data
    ).then(
        // console log the data that was given via an argument
    ).catch(
        // Handle any error (just console log out)
    ).finally(
        // Console log something, to show that this runs no matter what.
    );
}
















// A conditional statement (in programming) tells an application to execute 
// a block of code if the given boolean expression is true.

// Put another way, conditional statements allow our application to make decisions.




















// The if statement is the most basic (and most used) conditional statement 
// provided by javascript.


/* Here is the anatomy of an if statement.

if (<boolean expression>) 
{
    <lines of code that will run, if the boolean expression is true>   
}

*/




















// // Declare a name variable and assign it the value of 'Patrick'
// const myName = 'Patrick';

// // The if statement below tests if the myName variable equals the value 'Patrick'. 
// // If it does, the code located between the { }'s will execute.  If the expression
// // is not true, the block of code will not execute
// if (myName === 'Patrick') {
//     console.log('Hello ' + myName + '!');
//     console.log('How are you doing?!');
// }

// // Log a message to the console indicating the script has completed running
// console.log('End of program');

//  Note: demonstrate the if statement if name doesn't equal 'Patrick'
//  Note: Demonstrate replacing the conditional statement with simply 'true' or 'false'
//  Note: Demonstrate adding additional lines of code in the conditional block

// Exercise:

//  1. Author a new javascript and html file and link them together using the <script> tag

//  2. Include the following h1 tag in the <body> of your html file.
//
//      <h1 id="notice">You must be atleast 90 years old to enter this page!</h1>

//  3. In your javascript file, declare a `userAge` variable and assign it the number 90

//  4. Declare another variable named `notice` and set it equal to the output of
//      document.querySelector('#notice'), which searches for an html element
//      where id="notice".

//  5. Add an `if` statement to check if the `userAge` variable is greater-than 
//      or equal to 90.

//  6. If the userAge variable is 90 or greater set notice.innerHTML = "Age verified!".

//  7. Open VSCode and your web browser side-by-side (while running your app live-server)
//      and update the `userAge` variable to see the change in your web browser.




























// // if/else-if blocks
// //
// // There are additional forms the the "if" statement that can be used to make
// // complex decisions.
// //
// // For example, we might want an if statement to decide to between 2 (or more)
// // blocks of code based on boolean expressions.

// const h1 = document.querySelector('h1');
// const birthYear = 2000;

// // If the birth year is between 1965 and 1980 they are a Gen-Xer
// if(birthYear > 1965 && birthYear <= 1980) 
// {
//     h1.innerHTML = 'You are a Gen-Xer';

// // If the birth year is between 1981 and 1996 they are a Millenial
// } 
// else if (birthYear > 1980 && birthYear <= 1996) 
// {
//     h1.innerHTML = 'You are a Millenial';

// } 
// // Note: Add a 3rd condition for gen-y as an example
// // Note: Explain that even if multiple conditions match, javascript will only
// //          execute on the first block that it calculates to be true



















// Exercise: Modify your previous age validation example

// 1. Open the exercise from the previous lesson (the one where we check the users age)

// 2. Alter your existing if statement so it has an additional "else if" clause (block)

// 3. In the else-if clause check if `userAge` is greater than or equal to 80

// 4. If userAge is greater than or equal to 80 set 
//      notice.innerHTML = "You are nearly old enough, but not quite."

// 5. Launch your application with live-server and place VSCode and your web browser 
//     side-by-side.

// 6. Test your application by setting the `userAge` variable to be 90 in your code
//      and saving the file.  Then test again by setting userAge to be 80 and saving.

// Question:
//   When you test this script with `userAge` set to 90... why doesn't the
//     app execute both if/if-else blocks, even though both are true? 


















// if/else-if/else blocks

// There is one more form of the `if` statement.  This form includes an 
// `else` block.

// The else block is executed when none of the other `if` or `if-else`
// blocks are evaluated as being true. This means you can think of the
// `else` block as being the default option when nothing else matches.

const userTypedPassword = 'wrongPassword';
const h1 = document.querySelector('h1');

if (userTypedPassword === 'SpaceBallsFan87')
{
    h1.innerHTML = 'Access granted, welcome SpaceBallsFan87!';
}
else if (userTypedPassword === '12345') 
{
    h1.innerHTML = 'Access granted, welcome LordHelmet!';
}
else
{
    h1.innerHTML = 'Access denied!';
}

// NOTE: Demonstrate adding an else-if clause






















// Homework
//
// 1. Author a javascript and html file and link them together using the <script> tag

// 2. In the HTML file, add an <h1 id="notice"></h1> block as shown.

// 3. Drawing from my previous example: Write a script that declares 2  
//      variables userTypedUsername and userTypedPassword and initialize
//      them with fake username and a fake password (to represent the what
//      our fake user may have typed)

// 4. Construct an if/if-else/else code block.

// 5. Use the if-clause to check if the userTypedUsername 
//      equals 'rick' && the password equals 'DarkHelmetRulz'.

// 6. If the if-clause is true, set 
//     notice.innerHTML = 'Login successful, welcome rick!' 

// 7. In the first else-if clause check that the userTypedUsername
//      equals 'bill' && the userTypedPassword equals 'LoneStarForever'.

// 6. If the first else-if-clause is true, set 
//     notice.innerHTML = 'Login successful, welcome bill!'

// 7. In the else clause set 
//     notice.innerHTML = 'Access denied! Please check your username or password'

// 8. Launch your app with live-server and play with the userTypedUsername and 
//      userTypedPassword variables to see if you can cause each of the 
//      "if/if-else/else" clauses to execute.


// Homework Part 2

// Pop onto youtube and search 'javascript switch statement' and watch some of the videos.
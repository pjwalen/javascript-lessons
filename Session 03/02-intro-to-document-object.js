





















// The document object is an object that comes built into your web browser.  
// It is used to query and manipuate the the HTML document our javascipt 
// is loaded with. DOM
























// // Use the document object to query the body element of the document.
// const body = document.querySelector('body');
// console.log(body);



// //   Note: Demonstrate the need for the defer attribute in the <script> tag




















// // Query the element with the id of "main_content"
// // Note: If your document doesn't contain an element with the id 'main_content', then it returns a null value.
// const main = document.querySelector('#main_content');

// // Populate the main element with some additional HTML content
// //      Note: Show the additional HTML elements in the elements tab of the browser
// main.innerHTML = '<h2>Main content</h2>';
 




// // Note: querySelector method uses CSS selector language to find whatever
// //          our required element is.






















// Exercise:
//  1. Author a new javascript and html file

//  2. In the HTML file include a main element with a unique id

//  3. In the javascript file define a greeting string variable with a greeting
//      i.e. "Hello world",  "How're you doing?"

//  4. Query for our main element by its ID and popluate its .innerHTML with 
//      a '<p>(your greeting variable)</p>'

//  5. Extra points for single-line comments explaining your work

//  6. Extra points for multi-line comment at the top of the file
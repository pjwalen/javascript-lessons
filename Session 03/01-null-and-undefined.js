


















// // A variable with the type 'undefined' is a variable that has been declared
// // but has not yet been assigned a value.
// let x;
// console.log('x = ' + x);


























// A variable that is a null type has been declared and it has been defined
// but it was defined with a lack a value.  For instance, if we queried a
// database and it wanted to let us know that there were no results, it 
// might return a null value.
let x = null;
console.log(x);






















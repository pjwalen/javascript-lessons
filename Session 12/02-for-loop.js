


























// In the previous loop examples (while and do-while) we started to notice a
// pattern emerge.  The way we most commonly used loops was to iterate over
// a known number of things (like elements of an array).  In doing this we
// inevitably follow a common pattern. 

//  1. We declare and initialize an index variable
//  2. We configure the condition in the loop to see if the index equals the
//      expected number of iterations (e.g. the length of an array)
//  3. We increment/decrement the index and start the loop again

// For-loops are basically taylor-made for this exact situation and provides
// standard way of writing index variables, conditions and 
// incrementing/decrementing operations.
































/* For-Loop pseudocode


Initialization - This is where we initialize our 'index'

Condition - This is where we declare the condition statement whose truthyness
              is checked at every iteration

Afterthought - This is the statement that is run at the end of every iteration (like index ++)





    for (<initializtion>; <condition>; <afterthought>) {
        const currentElement = array[index];

        <do something with the current element>;
    }



*/




// Note the semi-colons between the elements of the for-loop



































// // Basic for loop example
// const jsOutput = document.querySelector('#jsOutput');

// function displayFruit() {

//     const fruits = ["Apple", "Banana", "Orange", "Pear", "Kiwi"];

//     const unorderedList = document.createElement('ul');

//     for (let index = 0; index < fruits.length; index++) {
//         // Create a list item element
//         let listItem = document.createElement('li');

//         // Set the innerHTML of the listItem to the current fruit
//         listItem.innerHTML = fruits[index];

//         // Append the listItem to the unoderedList
//         unorderedList.appendChild(listItem);

//     }

//     // Append the unoderedList to jsOutput
//     jsOutput.appendChild(unorderedList);

// }


// displayFruit();


// NOTE the similarities to the while-loop.  We can do the same thing with a
//         while-loop, but the for-loop specifically accounts for the index.

























// Exercise construct an unordered list from the elements of an array, using a basic for-loop.
//          Make use of the document.createElement and document.appendElement methods.


































// // Basic for loop example with a break/continue statement
// const jsOutput = document.querySelector('#jsOutput');

// function displayEvenNumbers() {

//     const numbers = [4, 2, 5, 10, 21, 8];
//     const unorderedList = document.createElement('ul');

//     for (let index = 0; index < numbers.length; index++) {
//         if (numbers[index] % 2 !== 0) {
//             continue;
//         }

//         let listItem = document.createElement('li');
//         listItem.innerHTML = numbers[index];
//         unorderedList.appendChild(listItem);
//     }

//     jsOutput.appendChild(unorderedList);

// }


// displayEvenNumbers();




// NOTE replace the break with a continue to "fix" the bug








// Demonstrate that the parameters of a for loop are optional

// let count = 0;
// for (;;) {
//     // Added this so we don't trigger an infinite loop
//     count ++;
//     if (count > 100000) {
//         break;
//     }
//     console.log(count);
// }


// // NOTE a for-loop with not parameters is an infinite-loop




















// Homework: 

// Update the application from the dom-inputs section of class such that:

// 1. It uses a for-loop when rendering the unordered list

// 2. It includes another button named sort.  When the sort button is clicked the the elements added to the unorderedlist are sorted and re-rendered.
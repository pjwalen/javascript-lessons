























// New DOM topics

// The document.createElement() method creates and returns a new HTML element
//     object.  The new object is not part of the HTML document by default.


// The element.appendChild() method appends DOM elements (like those created
//     by .createElement()) to the innerHTML of the given element.

// The InputElement.value property allows us to read and write the text 
//     currently in the <input /> element.



// // NOTE Talk about the HTML document first
// const submitButton = document.querySelector('#submitButton');
// const clearButton = document.querySelector('#clearButton');
// const nameInput = document.querySelector('#nameInput');
// const jsOutput = document.querySelector('#jsOutput');


// function submitHandler(event) {

//     // Create a brand new HTML object/element from thin-air
//     const newListItem = document.createElement('li');

//     // Set the value of its innerHTML
//     newListItem.innerHTML = nameInput.value;

//     // Append the object to our existing jsOutput list.
//     jsOutput.appendChild(newListItem);

// }

// function clearHandler(event) {
//     jsOutput.innerHTML = '';
// }

// // Attach submitHandler to the click event of the submitButton.
// submitButton.addEventListener('click', submitHandler);
// clearButton.addEventListener('click', clearHandler);




// NOTE a problem with this application... Once the user submits a new name,
//        our application doesn't maintain an array of that data to 
//        reference later.

























// Homework/Exercise Part 1: Copy the above program and try to make it work in your development environment






























// Version 2.0 of our program, but now we store our data in an array

const submitButton = document.querySelector('#submitButton');
const clearButton = document.querySelector('#clearButton');
const nameInput = document.querySelector('#nameInput');
const jsOutput = document.querySelector('#jsOutput');


// Define an array to store the data we're presenting to the user.
const namesArray = [];


// Define a function for rendering the data in the namesArray
function render() {
    jsOutput.innerHTML = '';

    let index = 0;
    // Loop through all of the elements in the namesArray and create a
    //      list item for each
    while (index < namesArray.length) {
        // Create a brand new HTML object/element from thin-air
        const newListItem = document.createElement('li');

        // Set the value of its innerHTML
        newListItem.innerHTML = namesArray[index];

        // Append the object to our existing jsOutput list.
        jsOutput.appendChild(newListItem);

        // Increment the index
        index ++;
    }
}


// Now when a user submits 
function submitHandler(event) {
    namesArray.push(nameInput.value);
    nameInput.value = '';
    render();
}


function clearHandler(event) {
    // Empty the namesArray by setting its length to 0
    namesArray.length = 0;
    render();
}


// Attach submitHandler to the click event of the submitButton.
submitButton.addEventListener('click', submitHandler);
clearButton.addEventListener('click', clearHandler);








// Homework/Exercise Part 2: Alter your original program so it stores our data
//       in an array and renders the output when it is updated.